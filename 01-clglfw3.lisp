;; This program demonstrates opening a window with cl-glfw3 (Opengl
;; ("the GL") does not provide the facility). A companion library like
;; GLFW (or SDL) is used to open a window and obtain a context (where
;; the GL's state is stored). The behavior is undefined if you try to
;; use the GL at all before you have a context.

(in-package :opengl-notes)

(glfw:def-window-size-callback update-viewport (window w h)
  (declare (ignore window))
  ;; Tell the GL how to scale triangles up from its internal 0.0-1.0
  ;; coordinate range, to match the size of the window.
  (gl:viewport 0 0 w h))

(defvar *currently-running* nil)
(defun empty-window ()
  (when *currently-running*
    (error "Running two instances has led to window system crashes."))
  (setf *currently-running* t)

  ;; Here we create our window and context.
  (glfw:with-init-window (:title "Empty Window Demonstration"
                                 :width 600 :height 400)
    (glfw:set-window-size-callback 'update-viewport)

    ;; GLFW windows default to being double-buffered. Let's try to
    ;; request vsync as well:
    (glfw:swap-interval 1)
    
    (loop
       (glfw:poll-events)             ; Process events, call callbacks
       (when (eq :press (glfw:get-key :escape glfw:*window*))
         (return))
       (gl:clear-color .2 .3 .3 1) ; Set the RGBA color for gl:clear to use.
       (gl:clear :color-buffer) ; Set the whole color buffer to the above.
       (glfw:swap-buffers)))
  
  (setf *currently-running* nil))
