;; Now let's draw a triangle. Depends on functions in the previous
;; file.

;; To draw triangles, we need to give the GL a number of vertices to
;; store in its memory, then tell it to draw some of them as
;; triangles. This will involve asking it to create two types of
;; objects:

;; We need at least one Vertex Buffer Object (VBO), which is basically
;; an array of numbers plus some metadata.

;; We also need at least one Vertex Array Object (VAO), which is
;; mostly a wrapper for a VBO. Or alternatively you could think of it
;; as a global state object, if you never change your current VAO.

;; You need at minimum one of each of these: the VBO to put your data
;; in, and the VAO because the GL says so. It's possible to have
;; several of each and to switch between them, but depending on your
;; card/drivers it's quite possible that using only one of each will
;; run faster, or alternating multiple VBOs with one VAO.

;; In any case, understand that a 3d model does not generally
;; correspond to a VAO or VBO, contrary to the impression you might
;; get. You should distribute the data for all models in use across
;; one or more VBOs in whatever way turns out to be efficient for the
;; scene being rendered.

;; We're going to assume here that a single VAO and a minimum of VBOs
;; is the safest option for performance.

(in-package :opengl-notes)

(defparameter *triangle-vertices*
  #(-0.5 -0.5 0.0  1.0 0.0 0.0
    0.5 -0.5 0.0    0.0 1.0 0.0
    0.0 0.5 0.0   0.0 0.0 1.0)
  "Three vertices, each with position and color data.")

;; A few words about vertex and fragment shaders. A vertex shader is
;; run for each input vertex, and its job is to take per-vertex data,
;; such as color, position, or texture coordinate, transform it if
;; necessary, and pass it on to the fragment shader stage. The
;; fragment shader is run at least once for each pixel a triangle
;; occupies, and its job is to take the vertex shader's output, which
;; has been interpolated, and produce a color. (And depth and stencil
;; values if needed.)

;; A shader which passes vertex positions and colors through
;; unchanged.
(defparameter *vertex-shader-source*
  "
#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 vertex_color;
out vec3 color;

void main()
{
    // We extend the position with another coordinate to get a
    // homogeneous coordinate.
    gl_Position = vec4(position, 1.0);
    color = vertex_color;
}")

;; A shader to pass the color through.
(defparameter *fragment-shader-source*
  "
#version 330 core
in vec3 color;
out vec3 FragColor;

void main()
{
    FragColor = color;
}")


(defun init-buffers ()
  ;; Allocate VAO. The GL context is a big state machine and generally
  ;; tracks a "current" instance of important types of objects, such
  ;; as VAOs, although more recently commands have been introduced to
  ;; operate on GL objects by name, rather than by setting the current
  ;; object before issuing the command. Many of these name-using
  ;; functions are bound by cl-opengl but aren't yet exported. Anyway,
  ;; GEN-VERTEX-ARRAY gives us a fresh name usable for a VAO (a bit
  ;; like GENSYM). BIND-VERTEX-ARRAY allocates a VAO for us,
  ;; associates it with that name, and makes it current, or just makes
  ;; it current if the name has already had a VAO allocated. NB. in
  ;; OpenGL parlance "bind" refers to making an object current
  ;; (binding it to the context, *not* to associating a name with a
  ;; value as in Lisp. Because we plan to use only one VAO, we're not
  ;; bothering to save its name.
  (gl:bind-vertex-array (gl:gen-vertex-array))
  
  (let* ((buffer (gl:gen-buffer)))
    ;; The pattern for the VBO is the same: obtain a name then bind to
    ;; allocate it. When we bind a buffer we specify the target, that
    ;; is, which of the various "slots" (in the Lisp sense) in the VAO
    ;; it should be bound to. For vertex data, we want :array-buffer.
    ;; (For basic use it's not necessary to know the other possible
    ;; targets.)
    (gl:bind-buffer :array-buffer buffer)

    ;; Now we tell the GL how to interpret the data we're going to put
    ;; in the buffer. Note that this information is stored in the VAO
    ;; and not the VBO itself, so any time we want to bind a
    ;; differently formatted VBO we'll need to repeat the following
    ;; calls modulo the details. Or we could maintain multiple VAOs,
    ;; but legend has it that switching VAOs is inefficient.

    ;; Take a look at *TRIANGLE-VERTICES*. That's the data we're going
    ;; to put into our VBO. It consists of 3 float coordinates in
    ;; space, then three denoting a color, and that layout is what we
    ;; need to describe to the GL with VERTEX-ATTRIB-POINTER. It needs
    ;; to know how those numbers should be grouped into arguments to
    ;; the vertex shader. Those arguments are known as generic vertex
    ;; attributes. The parameters to VERTEX-ATTRIB-POINTER are:
    ;;
    ;; The INDEX, indicating that the specified numbers will make up
    ;; the shader's zeroth argument, or the first, and so on.
    ;;
    ;; The SIZE and TYPE, here indicating that the argument to the
    ;; shader should be a vector of 3 floats.
    ;;
    ;; NORMALIZED has no relevance for floats.
    ;;
    ;; STRIDE indicates the offset in bytes from the start of one
    ;; triplet to the next. (From one position triplet to the next,
    ;; from one color triplet to the next.)
    ;;
    ;; Despite the name, POINTER is actually a byte offset into the
    ;; data, indicating the first component of the first generic
    ;; vertex attribute in the array.
    
    (let* ((type :float)
           (bytes (cffi:foreign-type-size type))
           (size 3)
           (stride (* 2 size bytes)))
      
      (gl:vertex-attrib-pointer 0 size type nil stride 0)
      (gl:vertex-attrib-pointer 1 size type nil stride
                                ;; The first color triplet follows the
                                ;; first position triplet.
                                (* bytes size)))

    ;; Tell the GL to pass these to vertex shaders.
    (gl:enable-vertex-attrib-array 0)
    (gl:enable-vertex-attrib-array 1)

    ;;; Load our triangle data into BUFFER.
    (let ((array (gl:alloc-gl-array :float (length *triangle-vertices*))))
      ;; Copying the data to a gl-array is just to get it into a
      ;; format that cl-opengl knows how to pass to OpenGL.
      (collect-ignore ; (a Series idiom)
       (setf (gl:glaref array (scan-range :from 0))
             (scan *triangle-vertices*)))

      ;; Use NAMED-BUFFER-DATA to allocate data storage for BUFFER.
      ;; Although cl-opengl doesn't export them, the named-* functions
      ;; are a bit nicer to use (the old-style function is
      ;; BUFFER-DATA). By passing (CFFI:NULL-POINTER) we indicate that
      ;; we don't want to copy any data into the allocated storage at
      ;; this time. The USAGE argument is a hint to the GL about how
      ;; we expect to use this data, to help it optimize.
      (%gl:named-buffer-data buffer
                             (gl:gl-array-byte-size array)
                             (cffi:null-pointer)
                             :static-draw)
      
      ;; Copy the data into the buffer. In this program, we've only
      ;; allocated enough storage for the triangle model and we could
      ;; have asked NAMED-BUFFER-DATA to copy it for us. But in
      ;; general it's more useful to separate allocating and
      ;; populating the buffer. Perhaps we have multiple models to
      ;; store in that buffer, or perhaps we are reading chunks of
      ;; data from a file and sending them to the GL as we go.
      (%gl:named-buffer-sub-data buffer
                                 0 ; destination offset
                                 (gl:gl-array-byte-size array) ; bytes to copy
                                 (gl::gl-array-pointer array)) ; source
      (gl:free-gl-array array))

    
    buffer))


(defun compile-shader-program (vertex-shader-source fragment-shader-source)
  ;; To use the two shaders at the top of the file, we need to create
  ;; an object for each, set its source code, compile both, attach
  ;; them to a shader program, and link that program.
  (let ((program (gl:create-program)))
    (flet ((compile-and-attach (type source)
             (let ((shader (gl:create-shader type)))
               (gl:shader-source shader source)
               (gl:compile-shader shader)
               (let ((info (gl:get-shader-info-log shader)))
                 (when (plusp (length info))
                   (format t "; compiling ~a:~%~a" type info)))
               (gl:attach-shader program shader)
               shader)))
      
      (let ((vshader (compile-and-attach :vertex-shader vertex-shader-source))
            (fshader (compile-and-attach :fragment-shader fragment-shader-source)))
        (gl:link-program program)
        ;; After linking, shaders can be messed with or deleted
        ;; without affecting the program object.
        (gl:delete-shader vshader)
        (gl:delete-shader fshader)))
    program))

(defun triangle-window ()
  (when *currently-running*
    (error "Running two instances has led to window system crashes."))
  (unwind-protect
       (progn
         (setf *currently-running* t)

         ;; Let's ask GLFW to multisample and antialias our scene,
         ;; with :SAMPLES 4.
         (glfw:with-init-window (:title "Triangle Demonstration"
                                        :width 600 :height 400
                                        :samples 4)
           (glfw:set-window-size-callback 'update-viewport)
           
           ;; GLFW windows default to being double-buffered. Let's try to
           ;; request vsync as well:
           (glfw:swap-interval 1)

           (init-buffers)
           (let ((program (compile-shader-program
                           *vertex-shader-source*
                           *fragment-shader-source*)))
             (gl:use-program program)
             (loop
                (glfw:poll-events)    ; Process events, call callbacks
                (when (eq :press (glfw:get-key :escape glfw:*window*))
                  (return))
                ;; Set the RGBA color for gl:clear to use.
                (gl:clear-color .2 .3 .3 1)
                ;; Set the whole color buffer to the above.
                (gl:clear :color-buffer)

                ;; With all the setup done above, it only takes one
                ;; draw call to display our triangle. Earlier, using
                ;; VERTEX-ATTRIB-POINTER and
                ;; ENABLE-VERTEX-ATTRIB-ARRAY, we described two
                ;; parallel arrays to the GL, one of positions and one
                ;; of colors, and enabled them. Drawing uses them
                ;; implicitly.
                (gl:draw-arrays :triangles 0 3)
                ;; The vertices are processed by the vertex shader,
                ;; then are assembled into triangles by the GL. The
                ;; assembled triangles are clipped then passed to the
                ;; rasterizer, which produces fragments for the pixels
                ;; they inhabit, which are in turn passed to our
                ;; fragment shader.
                ;;
                #+t ; Draw-arrays does something like this:
                (defun draw-arrays (mode first count)
                  (mapcar
                   #'fragment-shader
                   (rasterize
                    (assemble-primitives
                     mode
                     (apply #'mapcar #'vertex-shader
                            (list (subseq *vertex-positions* start (+ start length))
                                  (subseq *vertex-colors* start (+ start length))))))))
                
                (glfw:swap-buffers)))))
    
    (setf *currently-running* nil)))
