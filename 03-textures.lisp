;; Now let's texture our triangle. Depends on functions in the previous
;; file.

(in-package :opengl-notes)


;; To texture the triangle, we first add texture coordinates to the
;; triangle data. (We'll have to update the buffer setup below as
;; well.)
(defparameter *textured-triangle-vertices*
  #(-0.5 -0.5 0.0  1.0 0.0 0.0  0.0 0.0
    0.5 -0.5 0.0   0.0 1.0 0.0  1.0 0.0  
    0.0 0.5 0.0    0.0 0.0 1.0  0.0 1.0)
  "Three vertices, each with position and color data.")


(defparameter *triangle-texture-path*
  (merge-pathnames #p"lisplogo_flag2_256.png" *load-truename*))


;; The vertex shader only needs to be extended to pass texture
;; coordinates through to the fragment shader.
(defparameter *textured-vertex-shader-source*
  "
#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 vertex_color;
layout (location = 2) in vec2 vertex_texcoord;
out vec3 color;
out vec2 texcoord;

void main()
{
    gl_Position = vec4(position, 1.0);
    color = vertex_color;
    texcoord = vertex_texcoord;
}")


;; The fragment shader needs a "uniform" variable to hold a texture
;; sampler. The shader is invoked on each fragment being processed,
;; and the "in" and "out" variables give us the data for that
;; fragment. But uniform variables act as parameters to the rendering
;; call and don't change from fragment to fragment.
(defparameter *textured-fragment-shader-source*
  "
#version 330 core
in vec3 color;
in vec2 texcoord;
out vec4 FragColor;
uniform sampler2D tex;

void main()
{
    FragColor = vec4(color,1.0) * texture(tex,texcoord);
}")

(defun init-textured-tri-buffers ()
  (gl:bind-vertex-array (gl:gen-vertex-array))
  
  (let* ((buffer (gl:gen-buffer)))
    (gl:bind-buffer :array-buffer buffer)
    (let* ((type :float)
           (bytes (cffi:foreign-type-size type))
           (stride (+ (* 2 3 bytes)
                      (* 2 bytes))))
      (gl:vertex-attrib-pointer 0 3 type nil stride 0)
      (gl:vertex-attrib-pointer 1 3 type nil stride
                                ;; The first color triplet follows the
                                ;; first position triplet.
                                (* bytes 3))
      (gl:vertex-attrib-pointer 2 2 type nil stride
                                ;; And the texture coordinates follow that.
                                (* bytes 6)))
    ;; Tell the GL to pass these to vertex shaders.
    (gl:enable-vertex-attrib-array 0)
    (gl:enable-vertex-attrib-array 1)
    (gl:enable-vertex-attrib-array 2)

    ;; Load our triangle data into BUFFER.
    (let ((array (gl:alloc-gl-array :float (length *textured-triangle-vertices*))))
      ;; Copying the data to a gl-array is just to get it into a
      ;; format that cl-opengl knows how to pass to OpenGL.
      (collect-ignore                   ; (a Series idiom)
       (setf (gl:glaref array (scan-range :from 0))
             (scan *textured-triangle-vertices*)))
      (%gl:named-buffer-data buffer
                             (gl:gl-array-byte-size array)
                             (cffi:null-pointer)
                             :static-draw)
      (%gl:named-buffer-sub-data buffer
                                 0      ; destination offset
                                 (gl:gl-array-byte-size array) ; bytes to copy
                                 (gl::gl-array-pointer array)) ; source
      (gl:free-gl-array array))

    buffer))

(defun png-to-texture (path has-alpha-channel)
  "Load the PNG file at PATH into a newly created texture, and return
the name of that texture. HAS-ALPHA-CHANNEL specifies whether the pixel
data is :RGB or :RGBA."
  (let ((texture (gl:gen-texture)) ; Get a texture name
        ;; pngload seems like a pretty nice library for loading png
        ;; files. If we tell it to :FLATTEN then it will give us a
        ;; 1-dimensional array, and if we tell it :FLIP-Y it will
        ;; vertically flip the image to match the GL's convention.
        (png (pngload:load-file path :flatten t :flip-y t)))
    ;; The pattern for a texture is similar to the pattern for VBOs.
    ;; We bind the texture to allocate it. The TARGET controls whether
    ;; a 2-dimensional, 1-dimensional or what have you texture is
    ;; allocated. Texture operations on that TARGET operate on the
    ;; texture currently bound to it.
    (gl:bind-texture :texture-2d texture)
    (gl:tex-image-2d :texture-2d
                     0 ; load into the base level; this is not a mipmap
                     ;; Tell the GL that the texture it's storing is
                     ;; in the SRGB color space. See :FRAMEBUFFER-SRGB
                     ;; below. If your texture file is actually using
                     ;; a linear color space, this needs to be
                     ;; different.
                     :srgb
                     (pngload:width png) 
                     (pngload:height png)
                     0 ; must always be zero
                     (if has-alpha-channel :rgba :rgb) ; the format of the png
                     :unsigned-byte ; and the type of the png array
                     (pngload:data png))
    ;; There's another piece of state here that isn't obvious: the GL
    ;; provides numbered "texture units" (at least 80 of them in
    ;; OpenGL 4), one of which is implicitly active at any given time,
    ;; and has its own value for each target. Fragment shaders sample
    ;; from texture units. When we bind a texture, we're binding it to
    ;; the active texture unit (it can be bound in several). When we
    ;; render we'll worry about establishing the desired bindings; the
    ;; point of mentioning all this here is just to make it clear that
    ;; while there is an implicit texture unit we're binding this
    ;; texture to, we don't care except insofar as we should be aware
    ;; we're clobbering its previous binding.

    (gl:generate-mipmap :texture-2d) ; The GL can generate mipmaps for us.
    ;; Tell the GL to treat the texture like wallpaper , repeating in
    ;; both directions. -S and -T refer to the two texture
    ;; coordinates: we could wrap in only one direction if we wanted.
    (gl:tex-parameter :texture-2d :texture-wrap-s :repeat)
    (gl:tex-parameter :texture-2d :texture-wrap-t :repeat)
    ;; Tell the GL that textures which appear smaller on the screen
    ;; than their actual size should use nearest-neighbor lookup in
    ;; the appropriate mipmap.
    (gl:tex-parameter :texture-2d :texture-min-filter :nearest-mipmap-nearest)
    ;; And that textures which appear larger on the screen should use
    ;; linear interpolation.
    (gl:tex-parameter :texture-2d :texture-mag-filter :linear)
    texture))

(defun textured-triangle-window ()
  (when *currently-running*
    (error "Running two instances has led to window system crashes."))
  (unwind-protect
       (progn
         (setf *currently-running* t)
         (glfw:with-init-window (:title "Triangle Demonstration"
                                        :width 600 :height 400
                                        :samples 4)
           (glfw:set-window-size-callback 'update-viewport)
           (glfw:swap-interval 1)

           ;; You may be aware that it's easy to screw up the gamma of
           ;; your computer graphics. Computer monitors and file
           ;; formats like to allocate more of the available values to
           ;; representing light colors than dark ones, to save bits
           ;; (gamma compression). Adding compressed values gives odd
           ;; nonlinear results, so we have to undo the compression
           ;; before we do any arithmetic on colors. Luckily the GL
           ;; can simply be asked to do this. Enabling
           ;; :FRAMEBUFFER-SRGB tells the GL to re-compress the
           ;; picture on the way out. Supplying :SRGB as the internal
           ;; format, as we did above, tells the GL that the texture
           ;; is in compressed format and conversion needs to be done
           ;; when sampling.
           (gl:enable :framebuffer-srgb)
           
           (init-textured-tri-buffers)
           
           (let ((program (compile-shader-program
                           *textured-vertex-shader-source*
                           *textured-fragment-shader-source*))
                 (texture (png-to-texture *triangle-texture-path* t)))
             (gl:use-program program)
             (loop
                (glfw:poll-events)    ; Process events, call callbacks
                (when (eq :press (glfw:get-key :escape glfw:*window*))
                  (return))
                (gl:clear-color .2 .3 .3 1)
                (gl:clear :color-buffer)

                ;; Before any draw call using textures, we need to
                ;; bind the textures involved to texture units, and to
                ;; indicate which texture unit should goes which each
                ;; sampler in the shader.
                (gl:active-texture 0) ; we want to bind to texture unit 0
                (gl:bind-texture :texture-2d texture) ; actually bind
                ;; Our fragment shader includes a "uniform" variable.
                ;; We set uniforms like this. Uniforms are stored in
                ;; the shader program and looke dup by variable name,
                ;; and we use the UNIFORM family of functions to set
                ;; them. UNIFORMI sets integer uniforms, and here we
                ;; indicate that the variable named "texture" should
                ;; be set to 0 (for texture unit 0, which we bound our
                ;; texture to just prior).
                (gl:uniformi (gl:get-uniform-location program "texture") 0)
                
                (gl:draw-arrays :triangles 0 3)
                (glfw:swap-buffers)))))
    
    (setf *currently-running* nil)))
