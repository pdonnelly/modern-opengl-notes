(asdf:defsystem #:opengl-notes
  :description "Examples for modern OpenGL."
  :author "Paul Donnelly"
  :version "0.0.1"
  :serial t
  :depends-on (#:cl-opengl #:cl-glfw3 #:pngload #:series)
  :components ((:file "package")
               (:file "setup-series")
               (:file "01-clglfw3")
               (:file "02-triangle")
               (:file "03-textures")))
